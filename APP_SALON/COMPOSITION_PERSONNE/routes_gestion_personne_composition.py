# routes_gestion_personne_composition.py
# OM 2020.04.16 Gestions des "routes" FLASK pour la table intermédiaire qui associe les personne et les composition.

from flask import render_template, request, flash, session
from APP_SALON import obj_mon_application
from APP_SALON.COMPOSITION.data_gestion_composition import GestionComposition
from APP_SALON.COMPOSITION_PERSONNE.data_gestion_personne_composition import GestionCompositionPersonne


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /genres_films_afficher_concat
# Récupère la liste de tous les personne et de tous les composition associés aux personne.
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/personne_composition_afficher_concat/<int:id_personne_sel>", methods=['GET', 'POST'])
def personne_composition_afficher_concat (id_personne_sel):
    print("id_film_sel ", id_personne_sel)
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_composition = GestionCompositionPersonne()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionGenres()
            # Fichier data_gestion_composition.py
            data_personne_composition_afficher_concat = obj_actions_composition.personne_composition_afficher_data_concat(id_personne_sel)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data composition", data_personne_composition_afficher_concat, "type ", type(data_personne_composition_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_composition_afficher_concat:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données composition affichés dans GenresFilms!!", "success")
            else:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_genres_films" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_composition/personne_composition_afficher.html",
                           data=data_personne_composition_afficher_concat)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.21 Définition d'une "route" /gf_edit_genre_film_selected
# Récupère la liste de tous les composition du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des composition, ainsi l'utilisateur voit les composition à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_edit_personne_composition_selected", methods=['GET', 'POST'])
def gf_edit_personne_composition_selected ():
    if request.method == "GET":
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_composition = GestionComposition()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionGenres()
            # Fichier data_gestion_composition.py
            # Pour savoir si la table "t_genres" est vide, ainsi on empêche l’affichage des tags
            # dans le render_template(personne_composition_modifier_tags_dropbox.html)
            data_composition_all = obj_actions_composition.composition_afficher_data('ASC', 0)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données de la table intermédiaire.
            obj_actions_composition = GestionCompositionPersonne()

            # OM 2020.04.21 Récupère la valeur de "id_film" du formulaire html "personne_composition_afficher.html"
            # l'utilisateur clique sur le lien "Modifier composition de ce film" et on récupère la valeur de "id_film" grâce à la variable "id_film_genres_edit_html"
            # <a href="{{ url_for('gf_edit_genre_film_selected', id_film_genres_edit_html=row.id_film) }}">Modifier les composition de ce film</a>
            id_personne_composition_edit = request.values['id_personne_composition_edit_html']

            # OM 2020.04.21 Mémorise l'id du film dans une variable de session
            # (ici la sécurité de l'application n'est pas engagée)
            # il faut éviter de stocker des données sensibles dans des variables de sessions.
            session['session_id_personne_composition_edit'] = id_personne_composition_edit

            # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
            valeur_id_personne_selected_dictionnaire = {"value_id_personne_selected": id_personne_composition_edit}

            # Récupère les données grâce à 3 requêtes MySql définie dans la classe GestionGenresFilms()
            # 1) Sélection du film choisi
            # 2) Sélection des composition "déjà" attribués pour le film.
            # 3) Sélection des composition "pas encore" attribués pour le film choisi.
            # Fichier data_gestion_personne_composition.py
            # ATTENTION à l'ordre d'assignation des variables retournées par la fonction "genres_films_afficher_data"
            data_personne_composition_selected, data_personne_composition_non_attribues, data_personne_composition_attribues = \
                obj_actions_composition.composition_personne_afficher_data(valeur_id_personne_selected_dictionnaire)

            lst_data_personne_selected = [item['id_personne'] for item in data_personne_composition_selected]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_selected  ", lst_data_personne_selected,
                  type(lst_data_personne_selected))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les composition qui ne sont pas encore sélectionnés.
            lst_data_personne_composition_non_attribues = [item['id_composition'] for item in data_personne_composition_non_attribues]
            session['session_lst_data_personne_composition_non_attribues'] = lst_data_personne_composition_non_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_composition_non_attribues  ", lst_data_personne_composition_non_attribues,
                  type(lst_data_personne_composition_non_attribues))

            # Dans le composant "tags-selector-tagselect" on doit connaître
            # les composition qui sont déjà sélectionnés.
            lst_data_personne_composition_old_attribues = [item['id_composition'] for item in data_personne_composition_attribues]
            session['session_lst_data_personne_composition_old_attribues'] = lst_data_personne_composition_old_attribues
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_data_personne_composition_old_attribues  ", lst_data_personne_composition_old_attribues,
                  type(lst_data_personne_composition_old_attribues))

            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data data_personne_composition_selected", data_personne_composition_selected, "type ", type(data_personne_composition_selected))
            print(" data data_personne_composition_non_attribues ", data_personne_composition_non_attribues, "type ",
                  type(data_personne_composition_non_attribues))
            print(" data_personne_composition_attribues ", data_personne_composition_attribues, "type ",
                  type(data_personne_composition_attribues))

            # Extrait les valeurs contenues dans la table "t_genres", colonne "intitule_genre"
            # Le composant javascript "tagify" pour afficher les tags n'a pas besoin de l'id_genre
            lst_data_personne_composition_non_attribues = [item['plante'] for item in data_personne_composition_non_attribues]
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print("lst_all_genres gf_edit_genre_film_selected ", lst_data_personne_composition_non_attribues,
                  type(lst_data_personne_composition_non_attribues))

            # Différencier les messages si la table est vide.
            if lst_data_personne_selected == [None]:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_genres_films" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données composition affichées dans GenresFilms!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.21 Envoie la page "HTML" au serveur.
    return render_template("personne_composition/personne_composition_modifier_tags_dropbox.html",
                           data_composition=data_composition_all,
                           data_personne_selected=data_personne_composition_selected,
                           data_composition_attribues=data_personne_composition_attribues,
                           data_composition_non_attribues=data_personne_composition_non_attribues)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.26 Définition d'une "route" /gf_update_genre_film_selected
# Récupère la liste de tous les composition du film sélectionné.
# Nécessaire pour afficher tous les "TAGS" des composition, ainsi l'utilisateur voit les composition à disposition
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/gf_update_personne_composition_selected", methods=['GET', 'POST'])
def gf_update_personne_composition_selected ():
    if request.method == "POST":
        try:
            # Récupère l'id du film sélectionné
            id_personne_selected = session['session_id_personne_composition_edit']
            print("session['session_id_film_genres_edit'] ", session['session_id_personne_composition_edit'])

            # Récupère la liste des composition qui ne sont pas associés au film sélectionné.
            old_lst_data_personne_composition_non_attribues = session['session_lst_data_personne_composition_non_attribues']
            print("old_lst_data_personne_composition_non_attribues ", old_lst_data_personne_composition_non_attribues)

            # Récupère la liste des composition qui sont associés au film sélectionné.
            old_lst_data_personne_composition_attribues = session['session_lst_data_personne_composition_old_attribues']
            print("old_lst_data_genres_films_old_attribues ", old_lst_data_personne_composition_attribues)

            # Effacer toutes les variables de session.
            session.clear()

            # Récupère ce que l'utilisateur veut modifier comme composition dans le composant "tags-selector-tagselect"
            # dans le fichier "personne_composition_modifier_tags_dropbox.html"
            new_lst_str_personne_composition = request.form.getlist('name_select_tags')
            print("new_lst_str_personne_composition ", new_lst_str_personne_composition)

            # OM 2020.04.29 Dans "name_select_tags" il y a ['4','65','2']
            # On transforme en une liste de valeurs numériques. [4,65,2]
            new_lst_int_personne_composition_old = list(map(int, new_lst_str_personne_composition))
            print("new_lst_genres_films ", new_lst_int_personne_composition_old, "type new_lst_genres_films ",
                  type(new_lst_int_personne_composition_old))

            # Pour apprécier la facilité de la vie en Python... "les ensembles en Python"
            # https://fr.wikibooks.org/wiki/Programmation_Python/Ensembles
            # OM 2020.04.29 Une liste de "id_genre" qui doivent être effacés de la table intermédiaire "t_genres_films".
            lst_diff_composition_delete_b = list(
                set(old_lst_data_personne_composition_attribues) - set(new_lst_int_personne_composition_old))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_composition_delete_b ", lst_diff_composition_delete_b)

            # OM 2020.04.29 Une liste de "id_genre" qui doivent être ajoutés à la BD
            lst_diff_composition_insert_a = list(
                set(new_lst_int_personne_composition_old) - set(old_lst_data_personne_composition_attribues))
            # DEBUG bon marché : Pour afficher le résultat de la liste.
            print("lst_diff_composition_insert_a ", lst_diff_composition_insert_a)

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_composition = GestionCompositionPersonne()

            # Pour le film sélectionné, parcourir la liste des composition à INSÉRER dans la "t_genres_films".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_composition_ins in lst_diff_composition_insert_a:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_composition_ins" (l'id du genre dans la liste) associé à une variable.
                valeurs_personne_sel_composition_sel_dictionnaire = {"value_fk_personne": id_personne_selected,
                                                           "value_fk_composition": id_composition_ins}
                # Insérer une association entre un(des) genre(s) et le film sélectionner.
                obj_actions_composition.personne_composition_add(valeurs_personne_sel_composition_sel_dictionnaire)

            # Pour le film sélectionné, parcourir la liste des composition à EFFACER dans la "t_genres_films".
            # Si la liste est vide, la boucle n'est pas parcourue.
            for id_composition_del in lst_diff_composition_delete_b:
                # Constitution d'un dictionnaire pour associer l'id du film sélectionné avec un nom de variable
                # et "id_composition_del" (l'id du genre dans la liste) associé à une variable.
                valeurs_personne_sel_composition_sel_dictionnaire = {"value_fk_personne": id_personne_selected,
                                                           "value_fk_composition": id_composition_del}
                # Effacer une association entre un(des) genre(s) et le film sélectionner.
                obj_actions_composition.personne_composition_delete(valeurs_personne_sel_composition_sel_dictionnaire)

            # Récupère les données grâce à une requête MySql définie dans la classe GestionGenres()
            # Fichier data_gestion_composition.py
            # Afficher seulement le film dont les composition sont modifiés, ainsi l'utilisateur voit directement
            # les changements qu'il a demandés.
            data_personne_composition_afficher_concat = obj_actions_composition.personne_composition_afficher_data_concat(id_personne_selected)
            # DEBUG bon marché : Pour afficher le résultat et son type.
            print(" data composition", data_personne_composition_afficher_concat, "type ", type(data_personne_composition_afficher_concat))

            # Différencier les messages si la table est vide.
            if data_personne_composition_afficher_concat == None:
                flash(f"""Le film demandé n'existe pas. Ou la table "t_genres_films" est vide. !!""", "warning")
            else:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données composition affichées dans GenresFilms!!", "success")

        except Exception as erreur:
            print(f"RGGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGGF Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Après cette mise à jour de la table intermédiaire "t_genres_films",
    # on affiche les personne et le(urs) genre(s) associé(s).
    return render_template("pesonne_composition/personne_composition_afficher.html",
                           data=data_personne_composition_afficher_concat)
