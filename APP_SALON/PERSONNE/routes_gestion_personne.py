# routes_gestion_personne.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les personne.

import pymysql
from flask import render_template, flash, request
from APP_SALON import obj_mon_application
from APP_SALON.PERSONNE.data_gestion_personne import GestionPersonne

# OM 2020.04.16 Afficher un avertissement sympa...mais contraignant
# Pour la tester http://127.0.0.1:5005/avertissement_sympa_pour_geeks
@obj_mon_application.route("/avertissement_sympa_pour_geeks")
def avertissement_sympa_pour_geeks():
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("personne/AVERTISSEMENT_SYMPA_POUR_LES_GEEKS_films.html")




# OM 2020.04.16 Afficher les personne
# Pour la tester http://127.0.0.1:5005/films_afficher
@obj_mon_application.route("/personne_afficher")
def personne_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_personne = GestionPersonne()
            # Récupère les données grâce à une requête MySql définie dans la classe GestionFilms()
            # Fichier data_gestion_personne.py
            data_personne = obj_actions_personne.personne_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data personne", data_personne, "type ", type(data_personne))
            # Différencier les messages si la table est vide.
            if data_personne:
                # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                flash("Données personne affichées !!", "success")
            else:
                flash("""La table "t_films" est vide. !!""", "warning")
        except Exception as erreur:
            print(f"RGF Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGF Erreur générale. {erreur}","danger")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("personne/personne_afficher.html", data=data_personne)


# OM 2020.04.06 Pour une simple démo. On insère deux fois des valeurs dans la table personne
# Une fois de manière fixe, vous devez changer les valeurs pour voir le résultat dans la table "t_films"
# La 2ème il faut entrer la valeur du titre du film par le clavier, il ne doit pas être vide.
# Pour les autres valeurs elles doivent être changées ci-dessous.
# Une des valeurs est "None" ce qui en MySql donne "NULL" pour l'attribut "t_films.cover_link_film"
# Pour la tester http://127.0.0.1:5005/films_add
@obj_mon_application.route("/personne_add")
def personne_add():
    # obj_ma_db = MaBaseDeDonnee().__enter__()
    # print("obj_ma_db.open -->  ", obj_ma_db.open)
    # OM 2020.04.06 La connection à la BD doit être ouverte
    #if obj_ma_db.open:
    try:
        obj_actions_personne = GestionPersonne()
        valeurs_fixes_insertion_dictionnaire = {"value_nom": "Chibrax 100000",
                                                "value_prenom": "567",
                                                "value_mail": "oiu@dju.iiiii",
                                                "value_tel": "0000000000",
                                                'value_lieu': "LAUS"}
        obj_actions_personne.add_personne(valeurs_fixes_insertion_dictionnaire)
        # OM 2020.04.06 Entrée d'un titre de film au clavier pour les essais c'est mieux qu'une valeur aléatoire
        # Si l'utilisateur "claviote" seulement "ENTREE", alors on redemande de "clavioter" une chaîne de caractères
        #
        nom_personne_keyboard = None
        while not nom_personne_keyboard:
            nom_personne_keyboard = input("Titre du film ?")

        # Pour des essais il y a une valeur avec la valeur "None"... lorsqu'elle va être insérée en MySql
        # ce sera la valeur NULL
        valeurs_fixes_insertion_dictionnaire = {"value_nom": nom_personne_keyboard,
                                                "value_prenom": "567",
                                                "value_mail": "no.fail",
                                                "value_tel": None,
                                                'value_lieu': "1945-04-06"}
        obj_actions_personne.add_personne(valeurs_fixes_insertion_dictionnaire)
        flash("Ajout de 2 personne, OK !", "success")
        return render_template("home.html")
    except (Exception, pymysql.err.Error) as erreur:
        flash(f"FLASH ! Gros problème dans l'insertion de 2 personne  ! {erreur}", "danger")
        return render_template("home.html")
