 -- Database: chautems_melvin_salon_bd_104

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE if exists chautems_melvin_salon_bd_104;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS chautems_melvin_salon_bd_104;

-- Utilisation de cette base de donnée

USE chautems_melvin_salon_bd_104; 

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 05 Juin 2020 à 07:23
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `chautems_melvin_salon_bd_104`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_composition`
--

CREATE TABLE `t_composition` (
  `id_composition` int(11) NOT NULL,
  `plante` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qtt` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `t_composition`
--

INSERT INTO `t_composition` (`id_composition`, `plante`, `qtt`) VALUES
(1, 'oui?', '300'),
(3, 'jsp', '23'),
(4, 'paul', '29'),
(5, 'cacti', '2000'),
(6, 'mechant', ''),
(7, 'ouioui', '6969420'),
(8, 'SIENS', '360'),
(9, 'mechant', ''),
(10, 'test', '7');

-- --------------------------------------------------------

--
-- Structure de la table `t_composition_rdv`
--

CREATE TABLE `t_composition_rdv` (
  `id_composition_rdv` int(11) NOT NULL,
  `fk_composition` int(11) NOT NULL,
  `fk_rdv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `id_Personne` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lieu` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`id_Personne`, `nom`, `prenom`, `mail`, `tel`, `lieu`) VALUES
(1, 'Albert', 'LeVert', 'test@jij.ss', '089890980', 'mon chibre en plaine nature'),
(2, 'kassos', 'oui', 'a.cinq.dans.le.meme.trou@fml.com', '', ''),
(3, 'MODA', 'DIE', 'truffe@gggg.cccc', '', ''),
(4, 'JEAN', 'TERRUNMORT', 'RIP@straight.to.hell.com', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_composition`
--

CREATE TABLE `t_personne_composition` (
  `id_personne_composition` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `fk_composition` int(11) NOT NULL,
  `date_composition` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `t_personne_composition`
--

INSERT INTO `t_personne_composition` (`id_personne_composition`, `fk_personne`, `fk_composition`, `date_composition`) VALUES
(2, 4, 9, '2020-04-06 12:08:23'),
(5, 2, 8, '2020-04-07 06:02:36'),
(6, 2, 8, '2020-04-07 06:07:09');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_rdv`
--

CREATE TABLE `t_personne_rdv` (
  `id_personne_rdv` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `fk_rdv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_personne_technique`
--

CREATE TABLE `t_personne_technique` (
  `id_personne_technique` int(11) NOT NULL,
  `fk_personne` int(11) NOT NULL,
  `fk_technique` int(11) NOT NULL,
  `date_technique` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_rdv`
--

CREATE TABLE `t_rdv` (
  `id_rdv` int(11) NOT NULL,
  `date` date NOT NULL,
  `heure` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_soins`
--

CREATE TABLE `t_soins` (
  `id_soins` int(11) NOT NULL,
  `soins` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_soins_rdv`
--

CREATE TABLE `t_soins_rdv` (
  `id_soins_rdv` int(11) NOT NULL,
  `fk_soins` int(11) NOT NULL,
  `fk_rdv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_technique`
--

CREATE TABLE `t_technique` (
  `id_technique` int(11) NOT NULL,
  `technique` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `t_technique`
--

INSERT INTO `t_technique` (`id_technique`, `technique`) VALUES
(1, 'YAMETE_KUDASAI');

-- --------------------------------------------------------

--
-- Structure de la table `t_technique_rdv`
--

CREATE TABLE `t_technique_rdv` (
  `id_technique_rdv` int(11) NOT NULL,
  `fk_technique` int(11) NOT NULL,
  `fk_rdv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_composition`
--
ALTER TABLE `t_composition`
  ADD PRIMARY KEY (`id_composition`);

--
-- Index pour la table `t_composition_rdv`
--
ALTER TABLE `t_composition_rdv`
  ADD PRIMARY KEY (`id_composition_rdv`),
  ADD KEY `fk_composition` (`fk_composition`),
  ADD KEY `fk_rdv` (`fk_rdv`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`id_Personne`);

--
-- Index pour la table `t_personne_composition`
--
ALTER TABLE `t_personne_composition`
  ADD PRIMARY KEY (`id_personne_composition`),
  ADD KEY `fk_personne` (`fk_personne`),
  ADD KEY `fk_composition` (`fk_composition`);

--
-- Index pour la table `t_personne_rdv`
--
ALTER TABLE `t_personne_rdv`
  ADD PRIMARY KEY (`id_personne_rdv`),
  ADD KEY `fk_personne` (`fk_personne`),
  ADD KEY `fk_rdv` (`fk_rdv`);

--
-- Index pour la table `t_personne_technique`
--
ALTER TABLE `t_personne_technique`
  ADD PRIMARY KEY (`id_personne_technique`),
  ADD KEY `fk_personne` (`fk_personne`),
  ADD KEY `fk_technique` (`fk_technique`);

--
-- Index pour la table `t_rdv`
--
ALTER TABLE `t_rdv`
  ADD PRIMARY KEY (`id_rdv`);

--
-- Index pour la table `t_soins`
--
ALTER TABLE `t_soins`
  ADD PRIMARY KEY (`id_soins`);

--
-- Index pour la table `t_soins_rdv`
--
ALTER TABLE `t_soins_rdv`
  ADD PRIMARY KEY (`id_soins_rdv`),
  ADD KEY `fk_soins` (`fk_soins`),
  ADD KEY `fk_rdv` (`fk_rdv`);

--
-- Index pour la table `t_technique`
--
ALTER TABLE `t_technique`
  ADD PRIMARY KEY (`id_technique`);

--
-- Index pour la table `t_technique_rdv`
--
ALTER TABLE `t_technique_rdv`
  ADD PRIMARY KEY (`id_technique_rdv`),
  ADD KEY `fk_technique` (`fk_technique`),
  ADD KEY `fk_rdv` (`fk_rdv`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_composition`
--
ALTER TABLE `t_composition`
  MODIFY `id_composition` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `t_composition_rdv`
--
ALTER TABLE `t_composition_rdv`
  MODIFY `id_composition_rdv` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `id_Personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_personne_composition`
--
ALTER TABLE `t_personne_composition`
  MODIFY `id_personne_composition` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `t_personne_rdv`
--
ALTER TABLE `t_personne_rdv`
  MODIFY `id_personne_rdv` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_personne_technique`
--
ALTER TABLE `t_personne_technique`
  MODIFY `id_personne_technique` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_rdv`
--
ALTER TABLE `t_rdv`
  MODIFY `id_rdv` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_soins`
--
ALTER TABLE `t_soins`
  MODIFY `id_soins` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_soins_rdv`
--
ALTER TABLE `t_soins_rdv`
  MODIFY `id_soins_rdv` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_technique`
--
ALTER TABLE `t_technique`
  MODIFY `id_technique` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `t_technique_rdv`
--
ALTER TABLE `t_technique_rdv`
  MODIFY `id_technique_rdv` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_composition_rdv`
--
ALTER TABLE `t_composition_rdv`
  ADD CONSTRAINT `#fk_composition_rdv` FOREIGN KEY (`id_composition_rdv`) REFERENCES `t_composition` (`id_composition`),
  ADD CONSTRAINT `#fk_rdv_composition` FOREIGN KEY (`id_composition_rdv`) REFERENCES `t_rdv` (`id_rdv`);

--
-- Contraintes pour la table `t_personne_composition`
--
ALTER TABLE `t_personne_composition`
  ADD CONSTRAINT `#fk_composition_personne` FOREIGN KEY (`fk_composition`) REFERENCES `t_composition` (`id_composition`),
  ADD CONSTRAINT `#fk_personne_composition` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_Personne`);

--
-- Contraintes pour la table `t_personne_rdv`
--
ALTER TABLE `t_personne_rdv`
  ADD CONSTRAINT `#fk_personne_rdv` FOREIGN KEY (`id_personne_rdv`) REFERENCES `t_personne` (`id_Personne`),
  ADD CONSTRAINT `#fk_rdv_personne` FOREIGN KEY (`id_personne_rdv`) REFERENCES `t_rdv` (`id_rdv`);

--
-- Contraintes pour la table `t_personne_technique`
--
ALTER TABLE `t_personne_technique`
  ADD CONSTRAINT `#fk_personne_technique` FOREIGN KEY (`fk_personne`) REFERENCES `t_personne` (`id_Personne`),
  ADD CONSTRAINT `#fk_technique_personne` FOREIGN KEY (`fk_technique`) REFERENCES `t_technique` (`id_technique`);

--
-- Contraintes pour la table `t_soins_rdv`
--
ALTER TABLE `t_soins_rdv`
  ADD CONSTRAINT `#fk_rdv_soins` FOREIGN KEY (`id_soins_rdv`) REFERENCES `t_rdv` (`id_rdv`),
  ADD CONSTRAINT `#fk_soins_rdv` FOREIGN KEY (`id_soins_rdv`) REFERENCES `t_soins` (`id_soins`);

--
-- Contraintes pour la table `t_technique_rdv`
--
ALTER TABLE `t_technique_rdv`
  ADD CONSTRAINT `#fk_rdv_technique` FOREIGN KEY (`fk_rdv`) REFERENCES `t_rdv` (`id_rdv`),
  ADD CONSTRAINT `#fk_technique_rdv` FOREIGN KEY (`fk_technique`) REFERENCES `t_technique` (`id_technique`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
